---
marp: true
theme: uncover
---

<!-- _class: invert -->

![bg opacity grayscale](Assets/KiCad_PCB_Preview.jpg)

# Electronics for Artists 

<!-- _footer: © Krisjanis Rijnieks / Aalto Fablab / 2023  -->

---

# Files

https://gitlab.com/kriwkrow/electronics-for-artists

---

![bg fit](Assets/Arduino.jpg)

---

![bg fit left](Assets/Arduino.jpg)

# Arduino

Year: 2005
Place: Italy

---

![bg 60%](Assets/Breadboard.jpg)

---

![bg](Assets/BreadboardSpaghetti_01.jpg)
![bg](Assets/BreadboardSpaghetti_03.jpg)

---

![bg](Assets/BreadboardSpaghetti_02.jpg)

---

![bg](Assets/BreadboardSpaghetti_Art_01.jpg)

---

![bg](Assets/BreadboardSpaghetti_Art_02.jpg)

---

![bg](Assets/BreadboardSpaghetti_Art_03.jpg)

---

# Shields

---

![bg fit](Assets/Arduino-Shield-CNC.jpg)

---

![bg fit](Assets/Arduino-Shield-GSM.jpg)

---

![bg fit](Assets/Arduino-Shield-Relay.jpg)

---

# Fun PCB's

---

![bg 60%](Assets/Fun_PCB_Rockets.jpg)

<!-- _footer: © California STEAM / tindie.com -->

---

![bg 60%](Assets/Fun_PCB_Skull.jpg)

<!-- _footer: © California STEAM / tindie.com -->

---

![bg 60%](Assets/Fun_PCB_Bear.jpg)

<!-- _footer: © California STEAM / tindie.com -->

---

# Future

---

![bg](Assets/Microchip.jpg)

---

![bg](Assets/Integrated_Circuit.jpg)

---

![bg](Assets/Integrated_Circuit_Waffer.jpg)

<!-- _footer: © Guus Schoonewille 2013 -->

---

# Today: Fun PCB's

---

![bg left 60%](Assets/KiCad-Logo.jpg)

# KiCad

https://www.kicad.org/

---

![bg left 75%](Assets/Fab_Logo.jpg)

# Fab Library

https://gitlab.fabcloud.org/pub/libraries/electronics/kicad

---

![bg left 75%](Assets/Inkscape_Logo.jpg)

# Inkscape

https://inkscape.org/

---

# Workflow

1. Pick a shape
2. Capture schematic
3. Lay out PCB

---

# Demo

---

# PCB Production

---

![w:500](Assets/Logo_PCBWay.jpg)
https://www.pcbway.com/

---

![w:500](Assets/Logo_JLCPCB.jpg)
https://jlcpcb.com/

---

![w:500](Assets/Logo_Seed_Studio.jpg)
https://www.seeedstudio.com/

---

![bg](Assets/PCB_Milling.jpg)

---

# The End

---

<!-- _class: invert -->

# Electronics for Artists

Repository
https://gitlab.com/kriwkrow/electronics-for-artists

Krisjanis Rijnieks
https://rijnieks.com

Aalto Fablab
https://fablab.aalto.fi

<!-- _footer: © Krisjanis Rijnieks / Aalto Fablab / 2023  -->


