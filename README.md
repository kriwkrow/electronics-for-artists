# Electronics for Artists

Repository for the Electronics for Artists workshop. The main idea is to identify where we are in terms of electronics prototyping and adjust our flows to match the state-of-the-art.

## Description

Electronics for Artists is a 3+ hour workshop where participants learn the basics of PCB design and production. 

Arduino is there since 2005 and there was Wiring before that. One can say that using Arduino as prototyping practice is older than 20 years. Using the breadboard with Arduino has history too. Unfortunately, there are too many artists who do not move beyond breadboarding even in production mode. 

This workshop aims to demistify the core concepts of professional PCB design and encourage artists to use EDA software for their future projects.

## License

© Krisjanis Rijnieks / Aalto Fablab

If you want to use this repository and slides for your own purposes, reach out to me. I am not going to ask you money, but I would like to know if there is interest.
